#!/bin/bash

virtualenv venv --distribute
source venv/bin/activate

REQUIREMENT=${1:-requirements.txt}

if [ -f $REQUIREMENT ]; then
	pip install -r $REQUIREMENT
fi
