#!/bin/bash

PROJECT_DIR=${1:-$PWD}
cd $PROJECT_DIR

PROJECT_NAME=$(basename $PWD)
DATABASE_NAME=${PROJECT_NAME,,}

initdb pg

sudo chmod a+w /var/run/postgresql
postgres -D pg &

sleep 2

createdb $DATABASE_NAME

cat >> $PROJECT_NAME/settings.py <<EOF
import dj_database_url
DATABASES = {'default': dj_database_url.config(default='postgresql://localhost/$DATABASE_NAME') }
EOF

source venv/bin/activate
python manage.py syncdb

DB_PID=$(head -1 /var/run/postgresql/.s.PGSQL.5432.lock)
kill $DB_PID
