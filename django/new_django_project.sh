#!/bin/bash

if [ $# -lt 1 ]; then
    echo "Usage: $0 <PorjectName>"
    exit 1
fi

PROJECT_NAME=$1
mkdir -p $PROJECT_NAME
cd $PROJECT_NAME

virtualenv venv --distribute
source venv/bin/activate
pip install Django psycopg2 dj-database-url
pip freeze > requirements.txt
django-admin.py startproject $PROJECT_NAME .
cat "web: python manage.py runserver 0.0.0.0:$PORT --noreload" > Procfile
git init

cat > .gitignore <<EOF
*~
*.pyc
venv
pg
EOF

git add .
git commit -m "init django project"
