#!/bin/bash

DB_DIR=${1:-pg}
sudo chmod a+w /var/run/postgresql
echo "Running postgresql..."
postgres -D $DB_DIR
