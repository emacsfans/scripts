#!/bin/bash

printf "Your email address for git: "
read email
git config --global user.email $email

printf "Your name for git: "
read name
git config --global user.name $name
