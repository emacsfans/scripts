#!/bin/bash

OPENRESTY_VERSION=1.7.0.1
ZIP_NAME="ngx_openresty-${OPENRESTY_VERSION}.tar.gz"
DIR_NAME=${ZIP_NAME%.tar.gz}
DOWNLOAD_URL="http://openresty.org/download/$ZIP_NAME"
DOWNLOAD_DIR=~/Downloads
TARGET_DIR=~/Projects

mkdir -p $DOWNLOAD_DIR
wget -N -P $DOWNLOAD_DIR $DOWNLOAD_URL

mkdir -p $TARGET_DIR
if [ -n -d "$TARGET_DIR/$DIR_NAME"]; then
	tar xvf $DOWNLOAD_DIR/$ZIP_NAME -C $TARGET_DIR
fi

cd $TARGET_DIR/$DIR_NAME
./configure --with-luajit
make
sudo make install

echo "Done."
echo

echo "## Add executable to PATH:"
echo "export PATH=/usr/local/openresty/nginx/sbin:\$PATH"
echo
echo "## Start nginx:"
echo "nginx -p <DIR> -c <CONF>"
