#!/bin/bash

NGINX_WORKING_ROOT=~/Projects/nginx-config

mkdir -p $NGINX_WORKING_ROOT/logs

nginx -p $NGINX_WORKING_ROOT -c conf/nginx.conf
