#!/bin/bash

if [ $EUID -ne 0 ]; then
	echo "Should run as root. Please try again."
	exit 1
fi

DEVELOP_TOOLS="git openjdk-6-jdk openssh-server maven2"
apt-get install -y $DEVELOP_TOOLS
