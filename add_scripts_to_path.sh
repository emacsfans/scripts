#!/bin/bash

ROOT=$(cd "$(dirname "$0")"; pwd)
ENV_FILE=${1:-~/.bashrc}
ENV_FILE=$(cd "$(dirname "$ENV_FILE")"; pwd)/$(basename $ENV_FILE)

cd $ROOT
source add_self_to_path.sh

DIRS=$(find $ROOT -mindepth 1 -maxdepth 1 -type d -name "[^.]*")

for DIR in $DIRS; do
    cd $ROOT
    cd $DIR
    add_self_to_path.sh $ENV_FILE
done
