#!/bin/sh

REPOSITORIES="
git@heroku.com:cppdo.git
git@heroku.com:xiaohan.git
git@github.com:mfight/DotEmacs.git
"

for repository in $REPOSITORIES; do
	git clone $repository
done
