#!/bin/bash

HOME=/home/song

EDITORS="emacs vim"
BASIC_TOOLS="git gcc gcc-c++ kernel-devel wget" 
DEVELOP_TOOLS="expat-devel qt-devel qt-doc eclipse boost-devel cmake opencv-devel postgresql-devel python-devel postgresql-server ruby monodevelop gtk+-devel libXaw-devel libpng10-devel libtiff-devel flex compat-flex bison llvm-devel clang-devel man-pages telnet readline-devel pcre-devel openssl-devel maven ffmpeg graphviz"

GENERAL_SOFTWARE="libreoffice flash-plugin vlc gimp unrar bibus chm2pdf chmsee"

CPAN_PACKAGES="MD5 LWP"

function INSTALL_RPM_IF_NECESSARY {
	URL=$1
	DEST=$2
	SOFTWARE=$3
	echo "Installing $SOFTWARE..."
	if [ -z $URL ]; then
		echo "# No suitable package for $SOFTWARE."
		return
	fi
	if [ ! -f $DEST ]; then
		wget $URL -O $DEST
	fi
	yum -y -q install $DEST
}

if [ $EUID -ne 0 ]; then
	echo "Please run as root or use sudo. Exit"
	exit 1
fi


yum -y localinstall --nogpgcheck http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-stable.noarch.rpm http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-stable.noarch.rpm
yum -y update 
yum -y install $BASIC_TOOLS $EDITORS

DOWNLOAD_DIR=$HOME/Downloads/fedora_setup
mkdir -p $DOWNLOAD_DIR

cd $DOWNLOAD_DIR

if [ -n "$(uname -a | grep 'x86_64')" ]; then
	CHROME_URL="https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm"
	CHROME_DEST=google-chrome-stable_current_x86_64.rpm

	FLASH_URL="http://linuxdownload.adobe.com/adobe-release/adobe-release-x86_64-1.0-1.noarch.rpm"
	FLASH_DEST=adobe-release-x86_64-1.0-1.noarch.rpm

	if [ -n "$(uname -a | grep 'fc17')" ]; then
		VIRTUALBOX_URL="http://www.baidupcs.com/file/VirtualBox-4.2-4.2.4_81684_fedora17-1.x86_64.rpm?fid=4130003383-250528-2884258741&time=1353766379&sign=FPDTAE-DCb740ccc5511e5e8fedcff06b081203-E6Ev7J7WUhHPMqQvZcHH1ZEzcgo%3D&expires=8h&digest=43296caae863e095a7c83eede8c6a4d8&sh=1&response-cache-control=private"
		VIRTUALBOX_DEST=virtualbox_fedora_17_x86_64.rpm
	fi
elif [ -n "$(uname -a | grep 'i686')" ]; then
	if [ -n "$(uname -a | grep 'fc17')" ]; then
		VIRTUALBOX_URL="http://www.baidupcs.com/file/e5b3bcf21760e18b75f6c2bcb36e443f?fid=4130003383-250528-35202674&time=1355491375&sign=FDTA-DCb740ccc5511e5e8fedcff06b081203-q5pUuiVKI%2Bvb37jQtiCD%2BMzgLig%3D&expires=8h&sh=1&response-cache-control=private"
		VIRTUALBOX_DEST=virtualbox_fedora_17_i686.rpm
	fi
fi

INSTALL_RPM_IF_NECESSARY $CHROME_URL $CHROME_DEST "chrome"
INSTALL_RPM_IF_NECESSARY $VIRTUALBOX_URL $VIRTUALBOX_DEST "virtualbox"
INSTALL_RPM_IF_NECESSARY $FLASH_URL $FLASH_DEST "flash-plugin"

# Install pip and virtualenv for python
easy_install pip
pip install virtualenv

# Perl Packages
cpan $CPAN_PACKAGES

yum -y install $DEVELOP_TOOLS $GENERAL_SOFTWARE
