#!/bin/sh

DISTR=$(uname -a | awk '{print $2}')
GIT_REPO="git@bitbucket.org:emacsfans/scripts.git"
GIT_REPO_READONLY="https://emacsfans@bitbucket.org/emacsfans/scripts.git"

case $DISTR in
    "fedora")
	sudo yum -y install git
	;;
    "ubuntu")
	sudo apt-get -y install git
esac
    
git clone $GIT_REPO_READONLY
