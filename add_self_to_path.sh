#!/bin/bash

ENV_FILE=${1:-~/.bashrc}
ENV_FILE=$(cd "$(dirname "$ENV_FILE")"; pwd)/$(basename $ENV_FILE)
DATE=$(date "+%Y-%m-%d %H:%M")
PROGRAME=$(basename $0)

cat <<EOF >> $ENV_FILE
# Added by <$PROGRAME> at <$DATE>
export PATH="\$PATH:$PWD"

EOF

source $ENV_FILE
